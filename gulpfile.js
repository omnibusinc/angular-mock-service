var gulp                    = require('gulp');
var typescript              = require('gulp-typescript');
var sourcemaps              = require('gulp-sourcemaps');
var browserify              = require('gulp-browserify');
var runSequence             = require('run-sequence');

var rootPath = "./";
var buildpaths = {
  ts: ['./**/*.ts', '!./bower_components/**/*', '!./node_modules/**/*'],
  entry: 'angularmock.js'
};
var releasePaths = {
  root: './release'
};

gulp.task('typescript', function() {
  return gulp.src(buildpaths.ts)
    .pipe(sourcemaps.init())
    .pipe(typescript({
      "noImplicitAny" : false,
      "module" : "commonjs"
      }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(rootPath));
});

gulp.task('move-typescript', function() {
  return gulp.src(buildpaths.ts)
    .pipe(gulp.dest(releasePaths.root));
})

gulp.task('browserify', function() {
  return gulp.src(buildpaths.entry, {read: false})
    .pipe(browserify())
    .pipe(gulp.dest(releasePaths.root));
});

gulp.task('build', function() {
  runSequence(
    ['typescript', 'move-typescript'],
    'browserify'
  );
});

gulp.task("default", ["build"]);
