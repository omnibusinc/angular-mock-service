module Mock.models {
  'use strict';

  export class Mockable {
    properties : Array<any>;
  }
}
