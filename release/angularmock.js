(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var GUID = (function (_super) {
    __extends(GUID, _super);
    function GUID() {
        _super.apply(this, arguments);
    }
    return GUID;
}(string));
;

//# sourceMappingURL=GUID.js.map

},{}],2:[function(require,module,exports){
/// <reference path="typings/tsd.d.ts"/>
function logType(target, key) {
    var t = Reflect.getMetadata("design:type", target, key);
    target['properties'] = target['properties'] || {};
    target['properties'][key] = t.name;
}
var Mock;
(function (Mock) {
    var services;
    (function (services) {
        'use strict';
        var MockService = (function () {
            function MockService($q, $timeout) {
                var _this = this;
                this.$q = $q;
                this.$timeout = $timeout;
                this.mockClass = function (identifier, mockClass, count, completionCallback) {
                    var items = [];
                    _.forEach(mockClass.prototype.properties, function (type, name) {
                        items.push({ name: name, type: type });
                    });
                    return _this.mock(identifier, items, count, completionCallback);
                };
                this.mock = function (identifier, items, count, completionCallback) {
                    var deferred = _this.$q.defer();
                    var req = {
                        type: 'mock',
                        data: {
                            identifier: identifier,
                            completionCallback: completionCallback,
                            items: items,
                            count: count
                        }
                    };
                    _this.makeRequest(req, deferred).then(function (response) {
                        if (!!response.completionCallback) {
                            var data = {
                                results: response.results,
                                identifier: response.identifier
                            };
                            response.completionCallback(data);
                            deferred.resolve();
                        }
                    });
                    return deferred.promise;
                };
                this.makeRequest = function (req, deferred) {
                    var self = _this;
                    var results = [];
                    _this.$timeout(function () {
                        for (var i = req.data.count; i > 0; i--) {
                            var obj = {};
                            obj['id'] = (req.data.count - (req.data.count - i));
                            _.each(req.data.items, function (item) {
                                self.setItemValue(obj, item, req);
                            });
                            results.push(obj);
                        }
                        results = _.sortBy(results, 'id');
                        var data = {
                            status: 200,
                            results: results,
                            identifier: req.data.identifier,
                            completionCallback: req.data.completionCallback
                        };
                        deferred.resolve(data);
                    }, 500);
                    return deferred.promise;
                };
                this.getRandString = function (x) {
                    var s = "";
                    while (s.length < x && x > 0) {
                        var r = Math.random();
                        s += (r < 0.1 ? Math.floor(r * 100) : String.fromCharCode(Math.floor(r * 26) + (r > 0.5 ? 97 : 65)));
                    }
                    return s;
                };
                this.getGuid = function () {
                    var d = new Date().getTime();
                    var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                        var r = (d + Math.random() * 16) % 16 | 0;
                        d = Math.floor(d / 16);
                        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
                    });
                    return guid;
                };
                this.setItemValue = function (obj, item, req) {
                    var self = _this;
                    if (item.hasOwnProperty('default')) {
                        obj[item['name']] = item['default'];
                    }
                    else if (item.hasOwnProperty('options')) {
                        obj[item['name']] = _.sample(item['options']);
                    }
                    else if (item.hasOwnProperty('exclusiveOptions')) {
                        if (item['exclusiveOptions'].length > 0) {
                            obj[item['name']] = _.drop(_.shuffle(item['exclusiveOptions']));
                        }
                        else {
                            obj[item['name']] = null;
                        }
                    }
                    else {
                        switch (item['type'].toLowerCase()) {
                            case 'guid':
                                obj[item['name']] = self.getGuid();
                                break;
                            case 'string':
                                obj[item['name']] = "str_" + self.getRandString(8);
                                break;
                            case 'number':
                                obj[item['name']] = _.random(1, 1000);
                                break;
                            case 'boolean':
                                obj[item['name']] = Boolean(_.random(0, 1));
                                break;
                            case 'date':
                                obj[item['name']] = moment()
                                    .startOf('day')
                                    .add(_.random(1, 23), 'hours')
                                    .add(_.random(1, 59), 'minutes')
                                    .toDate();
                                break;
                            case 'array':
                                obj[item['name']] = [];
                                for (var i = 0; i < item['obj']['count']; i++) {
                                    obj[item['name']][i] = {};
                                    _.each(item['obj']['properties'], function (property) {
                                        self.setItemValue(obj[item['name']][i], property, req);
                                    });
                                }
                                break;
                        }
                    }
                };
            }
            MockService.$inject = ['$q', '$timeout'];
            return MockService;
        }());
        services.MockService = MockService;
        angular.module('Mock').service('MockService', MockService);
    })(services = Mock.services || (Mock.services = {}));
})(Mock || (Mock = {}));

//# sourceMappingURL=MockService.js.map

},{}],3:[function(require,module,exports){
var Mock;
(function (Mock) {
    var models;
    (function (models) {
        'use strict';
        var Mockable = (function () {
            function Mockable() {
            }
            return Mockable;
        }());
        models.Mockable = Mockable;
    })(models = Mock.models || (Mock.models = {}));
})(Mock || (Mock = {}));

//# sourceMappingURL=Mockable.js.map

},{}],4:[function(require,module,exports){
angular.module('Mock', []);
require('./Mockable');
require('./MockService');
require('./GUID');

},{"./GUID":1,"./MockService":2,"./Mockable":3}]},{},[4])