/// <reference path="typings/tsd.d.ts"/>

function logType(target : any, key : string) {
  var t = Reflect.getMetadata("design:type", target, key);
  target['properties'] = target['properties'] || {};
  target['properties'][key] = t.name;
}

module Mock.services {
  'use strict';
  export class MockService {
    static $inject = ['$q', '$timeout'];
    constructor(public $q: ng.IQService, public $timeout: ng.ITimeoutService) {}

    mockClass = (identifier: string, mockClass: (data?: Object) => any, count: number, completionCallback: (results?: any) => any) => {
      var items = [];
      _.forEach(mockClass.prototype.properties, function(type, name) {
        items.push({name: name, type: type});
      });
      return this.mock(identifier, items, count, completionCallback);
    };

    mock = (identifier: string, items: Array<Object>, count: number, completionCallback: (results?: any) => any) => {
      var deferred = this.$q.defer();
      var req = {
        type: 'mock',
        data: {
          identifier: identifier,
          completionCallback: completionCallback,
          items: items,
          count: count
        }
      };
      this.makeRequest(req, deferred).then(function(response) {
        if(!!response.completionCallback) {
          var data = {
            results: response.results,
            identifier: response.identifier
          }
          response.completionCallback(data);
          deferred.resolve();
        }
      });
      return deferred.promise;
    };

    makeRequest = (req: any, deferred: ng.IDeferred<any>) => {
      var self = this;
      var results = [];
      this.$timeout(function() {
        for(var i = req.data.count; i > 0; i--) {
          var obj = {};
          obj['id'] = (req.data.count - (req.data.count-i));
          _.each(req.data.items, function(item) {
            self.setItemValue(obj, item, req);
          });
          results.push(obj);
        }
        results = _.sortBy(results, 'id');
        var data = {
          status: 200,
          results: results,
          identifier: req.data.identifier,
          completionCallback: req.data.completionCallback
        };
        deferred.resolve(data);
      }, 500);
      return deferred.promise;
    };

    private getRandString = (x) => {
      var s = "";
      while(s.length<x&&x>0) {
          var r = Math.random();
          s+= (r<0.1?Math.floor(r*100):String.fromCharCode(Math.floor(r*26) + (r>0.5?97:65)));
      }
      return s;
    };

    private getGuid = () => {
      var d = new Date().getTime();
      var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
      });
      return guid;
    };

    private setItemValue = (obj: Object, item: Object, req: any) => {
      var self = this;
      if (item.hasOwnProperty('default')) {
        obj[item['name']] = item['default'];
      } else if(item.hasOwnProperty('options')) {
        obj[item['name']] = _.sample(item['options']);
      } else if(item.hasOwnProperty('exclusiveOptions')) {
        if(item['exclusiveOptions'].length > 0) {
          obj[item['name']] = _.drop(_.shuffle(item['exclusiveOptions']));
        } else {
          obj[item['name']] = null;
        }
      } else {
        switch(item['type'].toLowerCase()) {
          case 'guid':
            obj[item['name']] = self.getGuid();
            break;
          case 'string':
            obj[item['name']] = "str_" + self.getRandString(8);
            break;
          case 'number':
            obj[item['name']] = _.random(1, 1000);
            break;
          case 'boolean':
            obj[item['name']] = Boolean(_.random(0, 1));
            break;
          case 'date':
            obj[item['name']] = moment()
              .startOf('day')
              .add(_.random(1, 23), 'hours')
              .add(_.random(1, 59), 'minutes')
              .toDate();
            break;
          case 'array':
            obj[item['name']] = [];
            for(var i = 0; i < item['obj']['count']; i++) {
              obj[item['name']][i] = {};
              _.each(item['obj']['properties'], function(property) {
                self.setItemValue(obj[item['name']][i], property, req);
              });
            }
            break;
        }
      }
    }

  }
  angular.module('Mock').service('MockService', MockService);
}
