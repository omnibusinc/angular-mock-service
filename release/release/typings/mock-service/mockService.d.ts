/// <reference path='../angularjs/angular.d.ts' />

interface IMockService {
  mock(identifier: string, items: Array<Object>, count: number, completionCallback: (results?: any) => any): ng.IPromise<void>;
  mockClass(identifier: string, mockClass: any, count: number, completionCallback: (results?: any) => any): ng.IPromise<void>;
}
