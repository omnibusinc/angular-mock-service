# README #

## WORK IN PROGRESS ##

Mock Service for creating typed mock data using TypeScript reflection

### USE ###

* Any TypeScript classes that you want to mock data for will need to extend the Mockable class.
* The mockable properties in the class will need to have the @logType decorator applied to them.
* During transpilation, values are added to the 'properties' array on the object prototype (thanks to the Mockable class) describing the property names and types on the object.
* The MockService's 'MockClass' function iterates through the values added to the object's prototype and then passes them to the 'mock' function for further processing.
* the 'mock' function creates a deferred object, and then calls the 'makeRequest' function.
* The 'makeRequest' function mocks a service call, inside of which it loops through each of the mockable properties, checks the type for each, and creates the appropriate mock data per property.  When all the proeperties have had the requested number of entries created, the deferred object is resolved, and if a callback was provided, it is called and passed the mock data.

### TYPES  ###

* guid
* string
* number
* boolean
* date
* array

### OPTIONS ###

Defaults can be set for any property, as can options and exclusiveOptions.  The difference between options and exclusiveOptions is that the values in an options array can be used multiple times, whereas the values in an exclusiveOptions array can only be used once per set of mock data.

### EXAMPLE ###

# SelectDataSource.ts #

```javascript
export class SelectDataSource extends Mockable {
    @logType
    name: string;
    @logType
    value: string;
    @logType
    id: number;
  }
```

# Response.ts #
```javascript
export class Response {
    endpointName: string;
    url: string;
    results: any;
  }
```

# MyController.ts #
```javascript
export class MyController {
  SelectOptions: Array<models.SelectDataSource>;
  static $inject: string[] = ['MockService'];
  constructor(public MockService: IMockService) {
    this.MockService.mockClass('myEndpoint', models.SelectDataSource, 10, this.setSelectOptions);
  }
  setSelectOptions(data: models.Response) {
    this.SelectOptions = data.results;
  }
}
```

After the round trip has been made, this.SelectOptions will have 10 entries, each of which have a name and value properties which are strings, and an id property which is a number.