var Mock;
(function (Mock) {
    var models;
    (function (models) {
        'use strict';
        var Mockable = (function () {
            function Mockable() {
            }
            return Mockable;
        }());
        models.Mockable = Mockable;
    })(models = Mock.models || (Mock.models = {}));
})(Mock || (Mock = {}));

//# sourceMappingURL=Mockable.js.map
